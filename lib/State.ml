(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-34-37-39"]

module Make (S : sig
  type t
end) =
struct
  module Signature = struct
    type 'a t = |

    (* NYI *)
    let map f s = failwith "NYI"
  end

  module FreeState = Free.Make (Signature)
  include FreeState

  (* Define [Signature] so that ['a FreeState.t] is isomorphic to the
          following type:

     <<<
          type 'a t =
            | Return of 'a
            | Get of unit * (S.t -> 'a t)
            | Set of S.t * (unit -> 'a t)
     >>>
  *)

  let get () = failwith "NYI"

  let set s = failwith "NYI"

  let run m = failwith "NYI"
end
